package com.example.ffong.gps_example;


import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.security.Security;

//
// RingBuffer of coordinates, bearing, speed, time updated
// How to show RingBuffer? ( List or Table? )
// Show map?! How?
// Tab? (Map and List)?
//

public class MainActivity extends AppCompatActivity implements LocationItemFragment.OnFragmentInteractionListener, OnMapReadyCallback, LocationListener {

    private boolean mLocating;
    private boolean mInMapMode;
    private LocationItemFragment mHistFragment;
    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;
    private Location mLastLocation;
    private int mNextLocationId = 1;

    public MainActivity() {
        super();
        mLocating = false;
        mInMapMode = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // create listFragment
        mHistFragment = new LocationItemFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_layout, mHistFragment).commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);

        // show the Start / Stop Locating menu according to the current state
        MenuItem startLocatingItem = menu.findItem(R.id.action_start_locating);
        startLocatingItem.setVisible(!mLocating);
        MenuItem stopLocatingItem = menu.findItem(R.id.action_stop_locating);
        stopLocatingItem.setVisible(mLocating);

        // show Map / History menu according to the current state
        MenuItem showMapItem = menu.findItem(R.id.action_show_map);
        showMapItem.setVisible(!mInMapMode);
        MenuItem showHistoryitem = menu.findItem(R.id.action_show_history);
        showHistoryitem.setVisible(mInMapMode);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.action_start_locating) {
            // GPS available
            LocationManager locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (! locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
            else {
                try {
                    // get Last Known Location (by any means)
                    // uses default criteria?
                    String lastBestProvider = locManager.getBestProvider(new Criteria(), false);
                    LocationContent.clearItems();
                    mLastLocation = locManager.getLastKnownLocation(lastBestProvider);
                    if (mLastLocation != null) {
                        LocationContent.LocationItem locationItem = new LocationContent.LocationItem(String.valueOf(mNextLocationId), mLastLocation);
                        LocationContent.addItem(locationItem);
                        mNextLocationId++;
                    }
                    mHistFragment.dataUpdated();
                    locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
                    mLocating = true;
                    this.invalidateOptionsMenu();
                } catch (SecurityException se) {
                    // TBD show alert here!
                }
            }
            return true;
        }
        else if (id == R.id.action_stop_locating) {
            try {
                LocationManager locManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                locManager.removeUpdates(this);
                mLocating = false;
                this.invalidateOptionsMenu();
            }
            catch (SecurityException se) {
                // TBD show alert here
            }
            return true;
        }
        else if (id == R.id.action_show_map) {
            mMapFragment = new SupportMapFragment();
            mMapFragment.getMapAsync(this);
            getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_layout, mMapFragment).commit();
            mInMapMode = true;
            this.invalidateOptionsMenu();
            return true;
        }
        else if (id == R.id.action_show_history) {
            mHistFragment = new LocationItemFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_layout, mHistFragment).commit();
            mInMapMode = false;
            this.invalidateOptionsMenu();;
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String id) {
        // do nothing for now
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap; // save a handle
        LatLng coord;
        if (mLastLocation != null) {
            coord  = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }
        else {
            coord = new LatLng(-33.867, 151.206); // syndney

        }
        CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(coord, 13.0F);
        googleMap.moveCamera(camera);
        // remove old marker?!
        googleMap.addMarker(new MarkerOptions().position(coord));
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        LocationContent.LocationItem item = new LocationContent.LocationItem(String.valueOf(mNextLocationId), mLastLocation);
        LocationContent.addItem(item);
        mNextLocationId++;

        if (mInMapMode) {
            if (mMap != null) {
                // correct usage?
                mMap.clear();
                LatLng coord = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(coord, 18.0F);
                mMap.moveCamera(camera);
                // remove old marker?!
                mMap.addMarker(new MarkerOptions().position(coord));
            }
        }
        else {
            // refresh list fragment
            mHistFragment.dataUpdated();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        if (s.contentEquals(LocationManager.GPS_PROVIDER)) {
            switch (i) {
                case LocationProvider.OUT_OF_SERVICE:
                    // display at action bar?// Toast?
                    // stop requestLocationUpdates?
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    // display at action bar?// Toast?
                    // do nothing?
                    break;
                case LocationProvider.AVAILABLE:
                    // display at action bar?// Toast?
                    // resumed if location-updates were stopped.
                    break;
            }
        }
    }

    @Override
    public void onProviderEnabled(String s) {

        if (s.contentEquals(LocationManager.GPS_PROVIDER)) {
            // display at action bar? // Toast?
            // resume requestLocationUpdates?
        }

    }

    @Override
    public void onProviderDisabled(String s) {

        if (s.contentEquals(LocationManager.GPS_PROVIDER)) {
            // display at action bar?// Toast?
            // stop requestLocationUpdates?
        }
    }
}
