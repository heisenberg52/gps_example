package com.example.ffong.gps_example;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import android.location.Location;

/**
 * Created by f.fong on 9/2/2015.
 */
public class LocationContent {

    // Only store the most recent 20 locations
    private static final int MAX_LOCATIONS = 20;

    /**
     * An array of sample (dummy) items.
     */
    public static List<LocationItem> ITEMS = new ArrayList<LocationItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, LocationItem> ITEM_MAP = new HashMap<String, LocationItem>();

    public static void addItem(LocationItem item) {
        // remove before add
        if (ITEMS.size() == LocationContent.MAX_LOCATIONS) {
            int oldestItemIdx = ITEMS.size() - 1;
            LocationItem oldestItem = ITEMS.get(oldestItemIdx);
            ITEM_MAP.remove(oldestItem);
            ITEMS.remove(oldestItemIdx);
        }
        // insert at beginning (top of list)
        ITEMS.add(0, item);
        ITEM_MAP.put(item.id, item);
    }

    public static void clearItems() {
        ITEMS.clear();
        ITEM_MAP.clear();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class LocationItem {
        public String id;
        public Location location;

        public LocationItem(String id, Location content) {
            this.id = id;
            this.location = content;
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder;

            stringBuilder = new StringBuilder();

            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            cal.setTimeInMillis(location.getTime());
            stringBuilder.append("[");
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
            stringBuilder.append(sdf.format(cal.getTime()));
            stringBuilder.append("] ");
            stringBuilder.append(location.convert(location.getLongitude(), location.FORMAT_MINUTES));
            stringBuilder.append(", ");
            stringBuilder.append(location.convert(location.getLatitude(), location.FORMAT_MINUTES));
            if (location.hasBearing()) {
                stringBuilder.append(", " + String.valueOf(location.getBearing()) + "\u00B0");
            }
            if (location.hasSpeed()) {
                stringBuilder.append(", " + String.valueOf(location.getSpeed() * 3600.0F / 1000.0F ) + " kph");
            }


            return stringBuilder.toString();
        }
    }
}
